package data.scripts.world.exerelin.commandQueue;

// Credit: LazyWizard

public interface BaseCommand
{
    public void executeCommand();
}