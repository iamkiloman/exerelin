package data.scripts.world.exerelin;

public class EventBase
{
	private String type;

	public String getType()
	{
		return type;
	}

	public void setType(String value)
	{
		type = value;
	}
}
